#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

//Simpson Method
//Interate[Func[x],{x,a,b}]
double Simpson(double a, double b, double (*Func)(double), int n = 1000)
{
    if (n <= 0)
        n = 100;
    double x;
    double step = (b - a) / n;
    double result1 = 0.0;
    x = a;
    for (int i = 1; i < n; i++)
    {
        x += step;
        result1 += Func(x);
    }
    result1 *= 2;
    double result2 = 0.0;
    x = a + step / 2;
    for (int i = 0; i < n; i++)
    {
        result2 += Func(x);
        x += step;
    }
    result2 *= 4;
    double result = result1 + result2 + Func(a) + Func(b);
    result *= step / 6;
    return result;
}

//Limit[Integrate[1/(x^3 - 1), {x, -m, m}, PrincipalValue -> True], m -> Infinity]
double Func_test1(double x)
{
    return 1 / (x * x * x - 1);
}
double Func_test2(double x)
{
    return 1 / x;
}

int main()
{
    double result;
    double t = 0.01;
    result = Simpson(-1000, -1 - t, Func_test1) + Simpson(-1 + t, 1 - t, Func_test1) +
             Simpson(-1 + t, 1000, Func_test1);
    cout << setprecision(15) << result << endl
         << endl;
}