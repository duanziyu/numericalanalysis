#include<stdio.h> 
#include<math.h>  
#define MAX 1000 
int numn,numm;


double fac(int n){//n! 
    double f;  
    if(n==0||n==1)
        f=1;
    else 
        f=fac(n-1)*n;
    return f;  
}  
        
        
double hcf(double u,double v){//hcf==(u,v)  
    double r; 
    for(r=u;r>0;r--){
        if(v/r-(int)(v/r)==0&&u/r-(int)(u/r)==0)     
            return r;   
        }  
}  
    

double fact(int m,int n) {//(2n-2m)!/(n-2m)!  
    double a,b,c,temp,sum=1;  
    a=2*n-2*m;   
    b=n-2*m; 
    //suppose a>b  
    for(c=b+1;c<=a;c++) 
        sum=sum*c;  
    return sum;  
}


//Horner Algorithm
//double func(double array[],double x,int n){
//	double f=0.0;
//	for(int i=0;i<=n-1;i++)
//		f=(f+array[i])*x*x;
//	return f;
//}


//a=-1,b=1,Simpson Method
double fsimp(double eps, double (*P)(double)) 
{
	int n,k;
	double h,t1,t2,s1,s2,ep,p,x;
	n=1; h=2;
	t1=h*(P(-1)+P(1))/2.0; 
	s1=t1;
	ep=eps+1.0;
	while (ep>=eps){
		p=0.0;
  		for (k=0;k<=n-1;k++){
			x=-1+(k+0.5)*h;
   			p=p+P(x);
		}
		t2=(t1+h*p)/2.0;
  		s2=(4.0*t2-t1)/3.0;
  		ep=fabs(s2-s1);
  		t1=t2; s1=s2; n=n+n; h=h/2.0;
	}
    return(s2);
}
        

struct tag{
    double a[MAX];
}
Coefficient,Coem,Coen;
//$$
//P_n(x)=\sum^{[n/2]}_{m=0}(-1)^m*\frac {(2^n-2^m)!}{2^nm!(n-m)!(n-2^m)!}*x^{n-2m}
//$$
struct tag polynomial(int n){ 
    int i;   
    double A[MAX],B[MAX],C[MAX],D[MAX],b,c,m; 
    for(i=n/2;i>=0;i--) {   
        A[i]=pow(-1,i);//(-1)^m   
        b=fact(i,n);//(2n-2m)!/(n-2m)!    
        c=pow(2,n)*fac(i)*fac(n-i);//2^n*m!*(n-m)!   
        m=hcf(c,b);   
        B[i]=b/m;   
        C[i]=c/m;  
        D[i]=n-2*i; 
        Coefficient.a[n-2*i]=A[i]*B[i]/C[i];
        }   
    for(i=0;i<(n/2+1);i++){   
        if(i==0){    
            if(D[i]!=0&&B[i]!=1)//a_0      
                printf("%.0f/%.0fx^%.0f",A[i]*B[i],C[i],D[i]);     
            else{     
                if(D[i]==0&&B[i]==1)//n=0      
                    printf("%.0f",A[i]);      
                else if(D[i]!=0&&B[i]==1)          
                    printf("x");//n=1     
                else 
                    printf("%.0f/%.0f",A[i]*B[i],C[i]);
            }
        }
        else if(i%2==0){
            if(D[i]!=0&&D[i]!=1)//x^n(n>1)
                printf("+%.0f/%.0fx^%.0f",A[i]*B[i],C[i],D[i]);     
            else if(D[i]!=0&&D[i]==1)//x^1      
                printf("+%.0f/%.0fx",A[i]*B[i],C[i]);     
            else printf("+%.0f/%.0f",A[i]*B[i],C[i]);//x^0    
            } 
        else{    
            if(D[i]!=0&&D[i]!=1)         
                printf("%.0f/%.0fx^%.0f",A[i]*B[i],C[i],D[i]);//-x^n(n>1)    
            else if(D[i]!=0&&D[i]==1)      
                printf("%.0f/%.0fx",A[i]*B[i],C[i]);//-x^1     
            else 
                printf("%.0f/%.0f",A[i]*B[i],C[i]);//-x^0    
        }  
    }   
            printf("\n\n");
    return Coefficient;
}

//double f_n(double x){
//	double y=0;
//	int i;
//	struct tag Coen=polynomial(numn);
//	if(numn%2==0){
//		for(i=0;i<=numn/2;i++){
//			y+=Coen.a[i]*pow(x,numn-2*i);
//		}
//	}
//	else{
//		for(i=0;i<=(numn-1)/2;i++){
//			y+=Coen.a[i]*pow(x,numn-2*i);
//		}
//	}
//	return y;
//}


int main(){
	printf("please input n=");
	scanf("%d",&numn);
	printf("P_%d(x)=",numn);
	int i;
    double eps=0.0000001;
    double t;
    double final[numm];
    for(i=0;i<=numm;i++){
    	final[i]=0;
    }
    //suppose that numm>numn
    struct tag Coen=polynomial(numn);
    for(int i=0;i<=numn;i++){
    	printf("%f\t",Coen.a[i]);
    }
    printf("\n\n");
    printf("please input m=");
    scanf("%d",&numm);
    printf("P_%d(x)=",numm);
    struct tag Coem=polynomial(numm);
    for(i=0;i<=numm;i++){
    	printf("%f\t",Coem.a[i]);
    }
    printf("\n\n");
//    for(i=0;i<=numm+numn;i++){
//    	for(int j=0;j<i+1;j++){
//	    	final[i]+=Coem.a[j]*Coen.a[i-j];
//	    }
//    	printf("%f\t",final[i]);
//    }
    //integral
//    printf("\n the integral=");
    //double integral=0.0;
//    for(i=0;i<=numm;i++){
//  		if(final[i]!=0&&(i+1)/2!=0) 
//   			integral+=final[i]/(i+1)*2;
//   }
//    printf("%f",integral);
}




